#!/bin/bash

#limpa as informações de disco
st="Operacao iniciada em $(date '+%d/%m/%Y %H:%M:%S')"
printf "$st \n" &>>./dados_disco.txt
printf "$st \n" &>>./dados_uso.txt

#põe os comandos para executar simultaneamente
for cmd in "$@"; 
do {
	$cmd & pid=$!
	if [ -n "$PID_LIST" ];
       	then
		echo &>/dev/null
	else
		PID_LIST+=" $pid";
	fi
} done
 
trap "kill $PID_LIST" SIGINT

wait $PID_LIST
