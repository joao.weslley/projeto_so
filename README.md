Este projeto tem por objetivo manter os scripts necessários para a execução do projeto final da disciplina de Sistema Operacionais do grupo composto por Cauê de Araujo e João Weslley.

Aqui se encontram os scripts que coletarão os dados que utilizaremos para o nosso estudo.

O script 'diskinfo.sh' é responsável por anotar as informações sobre os disco. Ele salva os dados em um arquivo chamado 'dados_disco.txt';
O 'useinfo.sh' é responsável por anotar as informações sobre CPU e Memória. Os dados coletados por ele serão salvos no arquivo chamado dados_uso.txt;
O script 'dbscript.sh' é responsável por executar os demais scripts de forma simutânea.

**É expressadamente necessário que o primeiro script a ser enviado para o dbscript seja o *'useinfo.sh'*, caso contrário o script não irá funcionar corretamente e poderá ficar em execução infinitamente.**

Para executar os comandos use:
$bash dbscript.sh "bash useinfo.sh" "bash diskinfo.sh"
